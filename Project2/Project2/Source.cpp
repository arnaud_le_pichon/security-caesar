#include <iostream>
#include <string>
#include <cctype>
#include <algorithm>
#include <iostream>

void printCaesar(std::string mdp) {
	int iterator = 0;
	int offset = 1;
	std::string buff;
	std::transform(mdp.begin(), mdp.end(), mdp.begin(), toupper);

	while (offset < 26) {
		while (iterator < mdp.length()) {
			if ((mdp[iterator] + offset) > 'Z') {
				buff += 'A' + offset - ('Z' - mdp[iterator]) -1;
			}
			else {
				buff += mdp[iterator] + offset;
			}
			iterator++;
		}
		std::cout << "Decalage : " << offset << std::endl;
		std::cout << buff << std::endl;
		buff = "";
		iterator = 0;
		offset++;
	}
}

int main() {
	std::string mdp;
	while (true) {
		std::cout << "Veuillez saisir la phrase � dechiffrer : " << std::endl;
		std::cin >> mdp;
		printCaesar(mdp);
	}
}